
class axi_lite_write_test extends axi_lite_base_test;

  `uvm_component_utils(axi_lite_write_test)
  
  //---------------------------------------
  // sequence instance 
  //--------------------------------------- 
  //wr_rd_sequence seq;
    
  axi_lite_write_seq wseq;
  int data_r = 5;    //data_r --- data repetation
    //axi_agent.axi_sequencer.axi_master_write_seq.req.write_addr_i = 32'10;

  //---------------------------------------
  // constructor
  //---------------------------------------
   // function new(string name = "mem_wr_rd_test",uvm_component parent=null);
  function new(string name = "axi_lite_write_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    // Create the sequence
    wseq = axi_lite_write_seq::type_id::create("wseq");
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
    
    wseq.waddr   = 32'h0;
    wseq.wdata   = 128'b1;
    
    /*for(int i=0;i<data_r;i++) begin      
      wseq.waddr   = wseq.waddr + 4; 
      wseq.wdata   = 128'b1+i; 
      wseq.start(env.axi_agnt.sequencer);      
    end*/
    wseq.start(env.axi_agnt.sequencer);
    /*wseq.waddr   = 32'h0;
    wseq.wid     = 4'b0000;
    
    repeat(5) begin
      wseq.waddr   = wseq.waddr+4;    
      wseq.wdata   = wseq.wdata+1;    
      wseq.start(env.axi_agnt.sequencer); 
      
    end*/
    
    phase.drop_objection(this);
    
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 500);
    
  endtask : run_phase
  
endclass : axi_lite_write_test