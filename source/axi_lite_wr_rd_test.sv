
class axi_lite_wr_rd_test extends axi_lite_base_test;
  
  `uvm_component_utils(axi_lite_wr_rd_test)
  
  //---------------------------------------
  // sequence instance 
  ////--------------------------------------- 
  //wr_rd_sequence seq;
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "axi_lite_wr_rd_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
    //wrseq = axi_master_wr_rd_seq::type_id::create("wrseq");
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
    /*wseq.waddr=32'h4;
    
    rseq.raddr=32'h4;  
    
    for(int i=0;i<5;i++) begin
      wseq.start(env.axi_agnt.sequencer);
      rseq.start(env.axi_agnt.sequencer);     
    end*/
    
    wseq.waddr   = 32'h0;
    wseq.wdata   = 128'b1;
    
    rseq.raddr   = 32'h0;   
    
    /*for(int i=0;i<5;i++) begin   
      wseq.waddr   = wseq.waddr + 4;
      rseq.raddr   = wseq.waddr;
      wseq.wdata   = 128'b1+i;
      
      wseq.start(env.axi_agnt.sequencer);
      rseq.start(env.axi_agnt.sequencer);      
    end*/
    
    wseq.start(env.axi_agnt.sequencer);
    rseq.start(env.axi_agnt.sequencer);
    
  /*wseq.waddr   = 32'h0;    
    rseq.raddr   = 32'h0;
    
    repeat (5) begin
      wseq.waddr   = wseq.waddr + 4;
      rseq.raddr   = wseq.waddr;  
      wseq.start(env.axi_agnt.sequencer);
      rseq.start(env.axi_agnt.sequencer);
    end*/
    
    phase.drop_objection(this);
    
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 500);
  endtask : run_phase

endclass : axi_lite_wr_rd_test