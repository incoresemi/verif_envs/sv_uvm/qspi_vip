

`include "axi_seq_item.sv"
`include "axi_sequencer.sv"
`include "axi_sequence.sv"
`include "axi_driver.sv"
`include "axi_monitor.sv"
//`include "axi_master_wr_rd_seq.sv"
`include "axi_master_write_seq.sv"
`include "axi_master_read_seq.sv"

class axi_agent extends uvm_agent;

  //---------------------------------------
  // component instances
  //---------------------------------------
  axi_driver    driver;
  axi_sequencer sequencer;
  axi_monitor   monitor;

  `uvm_component_utils(axi_agent)
  
  //---------------------------------------
  // constructor
  //---------------------------------------
  function new (string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    
    monitor = axi_monitor::type_id::create("monitor", this);

    //creating driver and sequencer only for ACTIVE agent
    if(get_is_active() == UVM_ACTIVE) begin
      driver    = axi_driver::type_id::create("driver", this);
      sequencer = axi_sequencer::type_id::create("sequencer", this);
    end
    
  endfunction : build_phase
  
  //---------------------------------------  
  // connect_phase - connecting the driver and sequencer port
  //---------------------------------------
  function void connect_phase(uvm_phase phase);
    if(get_is_active() == UVM_ACTIVE) begin
      driver.seq_item_port.connect(sequencer.seq_item_export);
    end
  endfunction : connect_phase

endclass : axi_agent