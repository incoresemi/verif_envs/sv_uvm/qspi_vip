// Enabling Quad by 35h Command, but unable to Observe QE bit high in Read Status register with 05h command as the STATE signal 
//of Flash is going to BAD_CMD_STATE while writing into Flash.

`define QUADSPI_CR 		32'h00000000
`define QUADSPI_DCR 	32'h00000004
`define QUADSPI_SR		32'h00000008
`define QUADSPI_FCR 	32'h0000000c
`define QUADSPI_DLR		32'h00000010
`define QUADSPI_CCR 	32'h00000014
`define QUADSPI_AR		32'h00000018
`define QUADSPI_ABR		32'h0000001c
`define QUADSPI_DR		32'h00000020
`define QUADSPI_PSMKR	32'h00000024
`define QUADSPI_PSMAR	32'h00000028
`define QUADSPI_PIR		32'h0000002c
`define QUADSPI_LPTR	32'h00000030

class readStatusRegister extends axi_master_base_test;
  
  `uvm_component_utils(readStatusRegister)
  
  //---------------------------------------
  // sequence instance 
  ////--------------------------------------- 
  //wr_rd_sequence seq;
  
  axi_master_write_seq wseq;
  axi_master_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "readStatusRegister",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_master_write_seq::type_id::create("wseq");
    rseq = axi_master_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
      phase.raise_objection(this);
      
      wseq.length  = 4'b0000;
      wseq.awid    = 4'b0001;
      //wseq.wid     = 4'b0001;
	  rseq.length  = 4'b0000;
      rseq.arid    = 4'b0001;
	    
	 // sending address and data to QUADSPI_CR
	 wseq.waddr   = `QUADSPI_CR; 
	 wseq.wdata   = 32'h031f0f01; // Flash  25 MHz
	 wseq.wdata_v = 1;
	 #10; // 10ns delay
	 wseq.wdata_v = 0;
     wseq.start(env.axi_agnt.sequencer);
	   
	// sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001; 
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer);
	  
	wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
	
	//Enabling Quad mode
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;    
	wseq.wdata   = 32'h00000135;  // Enabling Quad by 35h command. But at this Command, STATE signal of flash is going to BAD_CMD_STATE.
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer);	
    
    //--------------------------------------------------
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	
	//---------------------------------------
	// Reading from Flash Started
	//---------------------------------------		
			
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; //  Reading 1 bytes data
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   =  32'h05200105; // Reading flash status Register.
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer);	
	     		
    
	//Reading data from flash.	
	
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR; //We are getting default values because 35h is not written on flash.
    rseq.start(env.axi_agnt.sequencer);  
    
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_sr_tcf);
	    		
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; // Flag Clear Register
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer);
		
	$display("readStatusRegister command test case completed");
	
	phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
	phase.phase_done.set_drain_time(this, 100);
  endtask : run_phase

endclass : readStatusRegister
