

`include "defines.sv"

class GangBlock_WriteProtected_4bytes_Quad extends axi_lite_base_test;
    
  `uvm_component_utils(GangBlock_WriteProtected_4bytes_Quad)
  
  //---------------------------------------
  // sequence instance 
  ////-------------------------------------
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "GangBlock_WriteProtected_4bytes_Quad",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);    
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);    
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	//wseq.waddr      = 32'1; 
	//wseq.wdata      = 32'0;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1; 
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01; // Enable QSPI 25 MHz 
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001; 
    wseq.start(env.axi_agnt.sequencer);
         
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
     
	 //sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000135;  //  Enable Quad mode
    wseq.start(env.axi_agnt.sequencer);
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	
	//sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h000003B7;  //  Enter 4 byte address mode
    wseq.start(env.axi_agnt.sequencer);
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	    
	 // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h0000037E;  //  Gang Block. this will set the dyb to 0.protecting the block/sector.
    wseq.start(env.axi_agnt.sequencer);
	
	
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);		  

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	//----------Implementing Gang block Scenario trying to erase on protected sector-----------------//
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000306;  //  
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
   
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
    // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00003F21;  //  Sectro Erase
    wseq.start(env.axi_agnt.sequencer);    
    
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR; 
	wseq.wdata   = 32'h01FF0000; 
    wseq.start(env.axi_agnt.sequencer);
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	   
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	   
	   // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000306;  //  WEL
    wseq.start(env.axi_agnt.sequencer);

	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	

    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 
	    
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h0000000F; // 16 bytes
    wseq.start(env.axi_agnt.sequencer);		
		
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h03003F12;    
    wseq.start(env.axi_agnt.sequencer);			
	   
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;   
    wseq.wdata   = 32'h01FF0000;
    wseq.start(env.axi_agnt.sequencer);		
	    
	// sending data to QUADSPI_DR_1
	wseq.waddr   = `QUADSPI_DR; 
    wseq.wdata   = 32'hff00ff00;  
    wseq.start(env.axi_agnt.sequencer);	

	// sending data to QUADSPI_DR_2
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff00;   
    wseq.start(env.axi_agnt.sequencer);
	   
	// sending data to QUADSPI_DR_3
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff00;    
    wseq.start(env.axi_agnt.sequencer);
    
    // sending data to QUADSPI_DR_4
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff00;    
    wseq.start(env.axi_agnt.sequencer);	   
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);		  

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 
    
    
	
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; // 1 byte
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h07000381;    // Read Ext read register 
	//wseq.wdata   = 32'h07003F81;    // Read Ext read register 
    wseq.start(env.axi_agnt.sequencer);	
	 			
	/* // sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR; 
	wseq.wdata   = 32'h00000000; 
	wseq.wdata_v = 1;
	#10;
	wseq.wdata_v = 0;
    wseq.start(env.axi_agnt.sequencer); */
         
		 #100000000;
    // Reading data from QUADSPI_DR                   
	rseq.raddr = `QUADSPI_DR;                    //FE  -- for both erase and write              
    rseq.start(env.axi_agnt.sequencer);           //FA -- for erase 
    

    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	       
    phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 100);
    
  endtask : run_phase

endclass : GangBlock_WriteProtected_4bytes_Quad 
