//Description: This Polling_Mode testcase describes 4 byte sector erase, write and Fast read and with dummy cycles. 
`include "defines.sv"

class Polling_Mode extends axi_lite_base_test;
    
  `uvm_component_utils(Polling_Mode)
  
  //---------------------------------------
  // sequence instance 
  ////-------------------------------------
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "Polling_Mode",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);    
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);    
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1;
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01;	 
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001;  
    wseq.start(env.axi_agnt.sequencer);
        
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);

	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;	
	wseq.wdata   = 32'h00000106;  //  WEL
    wseq.start(env.axi_agnt.sequencer);

    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);  	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);

	//---------------------------------------
	// Writing to Status register with 01 command
	//---------------------------------------	
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR;
	wseq.wdata   = 32'h00000000;  //  1 byte
    wseq.start(env.axi_agnt.sequencer); 
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;	  
	wseq.wdata   = 32'h01003501;  // Writing data to the Flash status register,  address on  single line 
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;  
	wseq.wdata   = 32'h00000000;
    wseq.start(env.axi_agnt.sequencer);	
								
	// sending data to QUADSPI_DR 1
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'h50000000;  // Set QE bit to 1 and BP2=1
    wseq.start(env.axi_agnt.sequencer);
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);  	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);		  

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 
	
	
	//---------------------------------------
	// Reading from Flash Started
	//---------------------------------------		
		
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000003; //  Reading 1 bytes dat
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   =  32'h09003505; // Reading flash status Register.
    wseq.start(env.axi_agnt.sequencer);	
	     		
    // sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;  
	wseq.wdata   = 32'h00000000;
	//wseq.wdata   = 32'h00010000;
    wseq.start(env.axi_agnt.sequencer);
	
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);  
    
   wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);  	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);
    
    $display("Polling_Mode command test case completed");	
    $finish;
	//#10000000;    
    phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 100);
    
  endtask : run_phase

endclass : Polling_Mode
