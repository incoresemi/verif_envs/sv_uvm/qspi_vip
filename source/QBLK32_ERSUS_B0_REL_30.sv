//Implementation of 32KB block erase suspend(B0h) and resume(30h) test case 

`include "defines.sv"

class QBLK32_ERSUS_B0_REL_30 extends axi_lite_base_test;
    
  `uvm_component_utils(QBLK32_ERSUS_B0_REL_30)
  
  //---------------------------------------
  // sequence instance 
  ////-------------------------------------
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "QBLK32_ERSUS_B0_REL_30",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);    
    super.build_phase(phase);

    // Create the sequence    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);    
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	//wseq.waddr      = 32'1; 
	//wseq.wdata      = 32'0;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1;	    
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01; // Enable Flash 25 MHz
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001; 
    wseq.start(env.axi_agnt.sequencer);
        
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000135;  // 
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000306;  // 
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	
	
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00003fDC;  // 
    wseq.start(env.axi_agnt.sequencer);
   
    // sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR; 
	wseq.wdata   = 32'h00010000; 
    wseq.start(env.axi_agnt.sequencer);	

    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	
	
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h000003b0;  
    wseq.start(env.axi_agnt.sequencer);
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);	
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	
	
	#100000000; //100us time taken for suspend operation

    // sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; 
    wseq.start(env.axi_agnt.sequencer);	
	
    // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR; 
	wseq.wdata   = 32'h07000348; 
    wseq.start(env.axi_agnt.sequencer);	
	
	// Reading data from QUADSPI_DR
	#10000000;	
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);  
    
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 

    wseq.start(env.axi_agnt.sequencer);		
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000330;  //  Sector resume
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	
	
	#400000000; //400us time taken for resume operation.
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));// sector erase  WIP	
	
// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR;
	wseq.wdata   = 32'h00000000;  //  1 byte
    wseq.start(env.axi_agnt.sequencer);
	
	 //sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h07000348;  //  Read Function Register 6 dummy cycles
    wseq.start(env.axi_agnt.sequencer);	
	
	// Reading data from QUADSPI_DR 1
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);  
    
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);		
	
	$display("BlockErase64KB_SuspendResume_Quad command test case completed");
	
    phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 100);
    
  endtask : run_phase

endclass : QBLK32_ERSUS_B0_REL_30