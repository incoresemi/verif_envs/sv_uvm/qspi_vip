

class axi_monitor extends uvm_monitor;

  //---------------------------------------
  // Virtual Interface
  //---------------------------------------
  virtual axi_if vif;
  
  bit [3:0] count = 4'b0000;

  //---------------------------------------
  // analysis port, to send the transaction to scoreboard
  //---------------------------------------
  uvm_analysis_port #(axi_seq_item) item_collected_port;
  
  //---------------------------------------
  // The following property holds the transaction information currently
  // begin captured (by the collect_address_phase and data_phase methods).
  //---------------------------------------
  axi_seq_item trans_collected;

  `uvm_component_utils(axi_monitor)

  //---------------------------------------
  // new - constructor
  //---------------------------------------
  function new (string name, uvm_component parent);
    super.new(name, parent);
    //trans_collected = new();
    item_collected_port = new("item_collected_port", this);
  endfunction : new

  //---------------------------------------
  // build_phase - getting the interface handle
  //---------------------------------------
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    if(!uvm_config_db#(virtual axi_if)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"});
  endfunction: build_phase
  
  //---------------------------------------
  // run_phase - convert the signal level activity to transaction level.
  // i.e, sample the values on interface signal ans assigns to transaction class fields
  //---------------------------------------
  virtual task run_phase(uvm_phase phase);
    forever begin
      trans_collected = new();
       
      @(posedge vif.aclk_i);
      wait(vif.write_start_i)begin 
        //$display("1vif.write_start_i= %d",vif.write_start_i);
        
          //Write Interface signals
          trans_collected.write_start_i   = vif.write_start_i;
          trans_collected.write_addr_i    = vif.write_addr_i;
          trans_collected.write_size_i    = vif.write_size_i;
          trans_collected.write_length_i  = vif.write_length_i;
          trans_collected.write_burst_i   = vif.write_burst_i;
          trans_collected.write_lock_i    = vif.write_lock_i;
          trans_collected.write_awid_i    = vif.write_awid_i;
          //trans_collected.write_wid_i     = vif.write_wid_i;       //commeneted as it is not ehrer in mkduumy new.
          trans_collected.write_data_i    = vif.write_data_i;
          trans_collected.write_datav_i   = vif.write_datav_i;
          trans_collected.write_strb_i    = vif.write_strb_i;
          
          
          //$display("monitor..vif.write_start_i= %d",vif.write_start_i);   
                    
          if(vif.write_length_i==0) begin
          wait(vif.write_ack_o);
          @(posedge vif.aclk_i);
          trans_collected.write_start_i    = vif.write_start_i;
          //$display("monitor..vif.write_ack_o= %d",vif.write_ack_o);
                   		
          // Write Response Channel Signals          
          
          wait(vif.write_done_o);
          @(posedge vif.aclk_i);
          trans_collected.write_ack_o        = vif.write_ack_o;
          trans_collected.write_data_req_o   = vif.write_data_req_o;
          trans_collected.write_done_o       = vif.write_done_o;
          trans_collected.write_err_o        = vif.write_err_o;
          trans_collected.write_bresp_o      = vif.write_bresp_o;
          trans_collected.write_bid_o        = vif.write_bid_o;
          trans_collected.write_bvalid_o     = vif.write_bvalid_o;
          //$display("monitor..vif.write_done_o= %d",vif.write_done_o);
                            
        end
        else begin 
          wait(vif.write_ack_o);
          @(posedge vif.aclk_i);
          trans_collected.write_start_i   = vif.write_start_i;
         
          for(int i =0;i<=(vif.write_length_i) ;i++) begin    
            @(posedge vif.aclk_i);
            if(vif.write_data_req_o) begin
              trans_collected.write_data_i  = vif.write_data_i;
              count  = count+4'b0001;
             // #10;
              trans_collected.write_data_i    = vif.write_data_i;
           
              //$display("count=%d",count); 
              end
              //$display("trans_collected.write_data_i=%h",trans_collected.write_data_i);
              //$display("mon..trans_collected.write_data_i=%h",trans_collected.write_data_i);  
            end
          end
        
          wait(vif.write_done_o);
          trans_collected.write_ack_o        = vif.write_ack_o;
          trans_collected.write_data_req_o   = vif.write_data_req_o;
          trans_collected.write_done_o       = vif.write_done_o;
          trans_collected.write_err_o        = vif.write_err_o;
          trans_collected.write_bresp_o      = vif.write_bresp_o;
          trans_collected.write_bid_o        = vif.write_bid_o;
          trans_collected.write_bvalid_o     = vif.write_bvalid_o;
        
        end
      //end
      
      
      //$display("Monitor read signals");
      wait(vif.read_start_i) begin
          //$display("read monitor..vif.read_start_i= %d",vif.read_start_i);
          //Read Interface signals
          trans_collected.read_arid_i     =   vif.read_arid_i;  
          trans_collected.read_addr_i     =   vif.read_addr_i;
          trans_collected.read_length_i   =   vif.read_length_i;
          trans_collected.read_size_i     =   vif.read_size_i ;
          trans_collected.read_burst_i    =   vif.read_burst_i; 
          trans_collected.read_lock_i     =   vif.read_lock_i ;  
          trans_collected.read_af_i       =   vif.read_af_i   ;
          //$display("monitor..vif.read_start_i= %d",vif.read_start_i);
         
          wait(vif.read_ack_o); 	
          trans_collected.read_ack_o      =  vif.read_ack_o;
         
      //master signals
      wait(vif.read_datav_o);  
      if(vif.read_datav_o) begin
        trans_collected.read_data_o       =  vif.read_data_o;
        trans_collected.read_datav_o      =  vif.read_datav_o;
        trans_collected.read_err_o        =  vif.read_err_o;
        trans_collected.read_done_o       =  vif.read_done_o;
        //$display("monitor..vif.read_data_o=%d",vif.read_data_o);
      end
      end
      item_collected_port.write(trans_collected);
    end
    
  endtask : run_phase

endclass : axi_monitor
