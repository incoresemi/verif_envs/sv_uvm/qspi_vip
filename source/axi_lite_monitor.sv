
class axi_lite_monitor extends uvm_monitor;

  //---------------------------------------
  // Virtual Interface
  //---------------------------------------
  virtual axi_lite_interface vif;

  //---------------------------------------
  // analysis port, to send the transaction to scoreboard
  //---------------------------------------
  uvm_analysis_port #(axi_lite_seq_item) item_collected_port;
  
  //---------------------------------------
  // The following property holds the transaction information currently
  // begin captured (by the collect_address_phase and data_phase methods).
  //---------------------------------------
  axi_lite_seq_item trans_collected;

  `uvm_component_utils(axi_lite_monitor)

  //---------------------------------------
  // new - constructor
  //---------------------------------------
  function new (string name, uvm_component parent);
    super.new(name, parent);
    //trans_collected = new();
    item_collected_port = new("item_collected_port", this);
  endfunction : new

  //---------------------------------------
  // build_phase - getting the interface handle
  //---------------------------------------
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    if(!uvm_config_db#(virtual axi_lite_interface)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"});
  endfunction: build_phase
  
  //---------------------------------------
  // run_phase - convert the signal level activity to transaction level.
  // i.e, sample the values on interface signal ans assigns to transaction class fields
  //---------------------------------------
  virtual task run_phase(uvm_phase phase);
  
    forever begin
	trans_collected = new();
    @(posedge vif.aclk_i);
    
    //$display("1vif.write_start_i= %d",vif.write_start_i);
        
    //Write Interface signals
    trans_collected.write_addr_i    = vif.write_addr_i;
    trans_collected.write_addrv_i   = vif.write_addrv_i;
    trans_collected.write_data_i    = vif.write_data_i;
    trans_collected.write_datav_i   = vif.write_datav_i;
    trans_collected.write_strb_i    = vif.write_strb_i; 

	//output signals 
	
    trans_collected.write_bresp_o      = vif.write_bresp_o;
    trans_collected.write_bvalid_o     = vif.write_bvalid_o;                    

    //$display("Monitor read signals");
    wait(vif.read_start_i) begin
    //$display("read monitor..vif.read_start_i= %d",vif.read_start_i);
    //Read Interface signals 
    trans_collected.read_start_i    =   vif.read_start_i; 
    trans_collected.read_addr_i     =   vif.read_addr_i; 
    trans_collected.read_af_i       =   vif.read_af_i   ;
    //$display("monitor..vif.read_start_i= %d",vif.read_start_i);
    end 
      //master signals
    wait(vif.read_datav_o);  
    if(vif.read_datav_o) begin
    trans_collected.read_data_o     =  vif.read_data_o;
    trans_collected.read_datav_o    =  vif.read_datav_o;
    //$display("monitor..vif.read_data_o=%d",vif.read_data_o);
    
    end
    item_collected_port.write(trans_collected);
    end
  endtask : run_phase

endclass : axi_lite_monitor
